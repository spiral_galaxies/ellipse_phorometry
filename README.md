# ellipse_phorometry

positional arguments:

    image                 Galaxy image


optional arguments:

	-h, --help            show this help message and exit
  
	--mask MASK           Mask image (zero pixels are good).
  
	--x-cen X_CEN         X-coordinate of galaxy center. Default: image center.
  
	--y-cen Y_CEN         Y-coordinate of galaxy center. Default: image center.
  
	--out-dir OUT_DIR     Directory with results
  
	--min-aper MIN_APER   Minimum semi-major size of the aperture [pix]. Default: 10 pixels.
  
	--max-aper MAX_APER   Maximum semi-major size of the aperture. Default: image limited aperture.
	
	--mag-zpt MAG_ZPT     Magnitude zeropoint. Default: value from the fits header.
  
    --outer-isophote OUTER_ISOPHOTE      Magnitude of an outer isophote. Default: no outer isophote.
	
	--aper-step APER_STEP   Aperture step in pixels. Default 5 pix.
  
	--make-model
  
	--scale SCALE         Scale arcsecs per pixel
