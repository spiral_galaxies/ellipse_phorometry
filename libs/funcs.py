#! /usr/bin/env python

from math import pi
from math import sin
from math import cos
from math import modf
import numpy as np


def get_inten_along_ellipse(ellipse, data, mask=None):
    """
    Function makes an azimuthal slice along the ellipse. The smoothing is
    performed by the averaging of several ellipses with a bit different radii
    """
    y_size, x_size = data.shape
    exc_anomaly = np.linspace(0, 2*pi, int(10*ellipse.sma))
    intensity = np.zeros_like(exc_anomaly)
    intensity_std = np.zeros_like(exc_anomaly)
    sinpa = sin(ellipse.pa)
    cospa = cos(ellipse.pa)
    cose = np.cos(exc_anomaly)
    sine = np.sin(exc_anomaly)
    q_value = 1 - ellipse.eps
    for i in range(len(exc_anomaly)):
        i_along_r = []
        for r in np.linspace(ellipse.sma-1, ellipse.sma+1, 3):
            fx, ix = modf(ellipse.x0 + r * cose[i] * cospa - r * q_value * sine[i] * sinpa)
            fy, iy = modf(ellipse.y0 + r * q_value * sine[i] * cospa + r * cose[i] * sinpa)
            iix = int(ix)
            iiy = int(iy)
            if (mask is not None) and (mask[iiy][iix] != 0):
                # This pixel is masked out
                continue
            if (iix < 1) or (iiy < 1) or (iix > x_size-2) or (iiy > y_size-2):
                # We are outside of the image frame. Skip this point
                continue
            i_along_r.append(((1.0-fx)*(1.0-fy)*data[iiy][iix] + fx*(1.0-fy)*data[iiy][iix+1] +
                              fy*(1.0-fx)*data[iiy+1][iix] + fx*fy*data[iiy+1][iix+1]))
        if len(i_along_r) != 0:
            intensity[i] = np.median(i_along_r)
            intensity_std[i] = np.std(i_along_r)
        else:
            intensity[i] = -9999
            intensity_std[i] = -9999
    intensity[intensity == -9999] = np.median(intensity[intensity != -9999])
    intensity_std[intensity_std == -9999] = np.median(intensity_std[intensity_std != -9999])
    return exc_anomaly, intensity, intensity_std


def is_ellipse_inside_frame(ellipse, x_size, y_size, margin=1):
    """
    Function checks if the given ellipse fits inside of the image
    with a given margin
    """
    exc_anomaly = np.linspace(0, 2*pi, int(10*ellipse.sma))
    sinpa = sin(ellipse.pa)
    cospa = cos(ellipse.pa)
    q_value = 1 - ellipse.eps
    x_ell = ellipse.x0 + ellipse.sma * np.cos(exc_anomaly) * cospa - ellipse.sma * q_value * np.sin(exc_anomaly) * sinpa
    y_ell = ellipse.y0 + ellipse.sma * q_value * np.sin(exc_anomaly) * cospa + ellipse.sma * np.cos(exc_anomaly) * sinpa

    x_min = min(x_ell)
    x_max = max(x_ell)
    y_min = min(y_ell)
    y_max = max(y_ell)

    if (x_min > margin) and (y_min > margin) and (x_max < x_size-margin) and (y_max < y_size-margin):
        return True
    return False
