#! /usr/bin/env python

from os import path
import subprocess
import numpy as np
from scipy.ndimage import minimum_position
from scipy.ndimage import maximum_position


work_dir = path.join(path.dirname(__file__), "../work_dir")


class SExCatalogue(object):
    def __init__(self, catFileName):
        self.objectList = []
        self.legend = []
        self.index = 0  # For iterations
        # Parse SE catalogue with arbitrary number of objects and parameters
        for line in open(catFileName):
            sLine = line.strip()
            if sLine.startswith("#"):
                # legend line
                self.legend.append(sLine.split()[2])
                continue
            params = [float(p) for p in line.split()]
            obj = {key: value for key, value in zip(self.legend, params)}
            self.objectList.append(obj)
        self.numOfObjects = len(self.objectList)

    def get_median_value(self, parameter):
        return np.median([obj[parameter] for obj in self.objectList])

    def get_all_values(self, parameter):
        return np.array([obj[parameter] for obj in self.objectList])

    def find_nearest(self, x_cen, y_cen):
        """
        Function returns the object from the catalogue which is closest to the given coordinates
        """
        x_centers = self.get_all_values(parameter="X_IMAGE")
        y_centers = self.get_all_values(parameter="Y_IMAGE")
        index_of_nearest_object = minimum_position(np.hypot(x_cen-x_centers, y_cen-y_centers))[0]
        nearest_object = self.objectList[index_of_nearest_object]
        return nearest_object

    def find_largest(self, parameter):
        index_of_largest = maximum_position(self.get_all_values(parameter=parameter))[0]
        largest_object = self.objectList[index_of_largest]
        return largest_object

    def __iter__(self):
        return self

    def __next__(self):
        """ Iteration method """
        if self.index < self.numOfObjects:
            self.index += 1
            return self.objectList[self.index-1]
        else:
            self.index = 1
            raise StopIteration


def call_sextractor(fits_name, config_name=None, params=None):
    """
    Function calls a SExtractor.
    Arguments:
        fits_name -- name of a fits file to analyse
        params -- additional params to override the values from the config file
    """
    config_name = path.join(path.dirname(__file__), "default.sex")
    params_file_name = path.join(path.dirname(__file__), "default.param")
    conv_file_name = path.join(path.dirname(__file__), "default.conv")
    # Check if files exist
    if not path.exists(fits_name):
        raise IOError("%s file not found" % fits_name)
    if not path.exists(config_name):
        raise IOError("%s file not found" % config_name)

    # Create a command line for SExtractor
    call_string = "sex %s" % fits_name
    call_string += " -c %s" % config_name
    call_string += " -PARAMETERS_NAME %s" % params_file_name
    call_string += " -FILTER_NAME %s" % conv_file_name
    if params is not None:
        for key, value in params.items():
            call_string += " -%s %s" % (key, value)

    # Call SExtractor
    rc = subprocess.call(call_string, shell=True)
    if rc == 0:
        # SExtractor call was successful
        return SExCatalogue("field.cat")
    else:
        print("Something went wrong during SExtractor call. Exiting.")
        exit(1)
