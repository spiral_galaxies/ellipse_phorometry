#!/usr/bin/env python

"""
Script for performing aperture photometry of galaxies via the fitting of ellipses and building
the growth curve.
"""

import pathlib
import argparse
from math import radians
from math import degrees
from types import SimpleNamespace
from os import makedirs

import numpy as np
from scipy.optimize import brentq
from scipy.interpolate import interp1d
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm

from astropy.io import fits
from photutils.isophote import EllipseGeometry
from photutils.isophote import Ellipse
from photutils.isophote import build_ellipse_model
from photutils.isophote import IsophoteList
from photutils import EllipticalAperture
from photutils import aperture_photometry

from libs.se import call_sextractor
from libs.funcs import is_ellipse_inside_frame
from libs.funcs import get_inten_along_ellipse


def load_fits(file_name):
    if file_name.exists():
        return fits.getdata(file_name)
    else:
        print("File %s does not exist." % file_name)
        exit(1)


def get_magzpt(file_name):
    header = fits.getheader(file_name)
    kw_list = ["M0", "m0", "MAGZPT", "magzpt"]
    for kw in kw_list:
        if kw in header:
            return float(header[kw])
    return None


def flux_to_mag(flux, magzpt):
    return -2.5 * np.log10(flux) + magzpt


def flux_to_surf_bri(flux_per_pixel, magzpt, pixel_scale):
    """
    Converts given flux in counts per pixel to surface brightness
    in magnitudes per square second.
    """
    flux_per_square_second = np.array(flux_per_pixel) / (pixel_scale ** 2)
    return -2.5 * np.log10(flux_per_square_second) + magzpt


def main(args):
    logfile = open("log.dat", "a", buffering=1)
    logfile.write("Starting with %s\n" % args.image)
    # Create the directory with results
    if not args.out_dir.exists():
        makedirs(args.out_dir)

    # Load images of the galaxy and mask
    galaxy_data = load_fits(args.image)
    y_size, x_size = galaxy_data.shape
    print("Loaded image with %i x %i pixels" % (x_size, y_size))

    if args.mask is not None:
        mask_data = load_fits(args.mask)
        print("Loaded mask (%i pixels are masked out).\n" % (len(np.where(mask_data != 0)[0])))
    else:
        mask_data = np.zeros_like(galaxy_data)
        print("No mask provided.")

    # Apply mask to the data
    galaxy_masked_data = np.ma.masked_array(galaxy_data, mask=mask_data)

    # Load magnitude zeropoint value from the header if not specified
    if args.mag_zpt is None:
        mag_zpt = get_magzpt(args.image)
    else:
        mag_zpt = args.mag_zpt

    # Prepare initial ellipse.
    # If center was not given, set it to the image centre
    x_cen = args.x_cen if args.x_cen is not None else x_size / 2.0
    y_cen = args.y_cen if args.y_cen is not None else y_size / 2.0

    # Call SExtractor to compute ellipse guess parameters
    print("Calling SExtractor... ", end="")
    catalogue = call_sextractor(args.image)
    print("Done\n")
    galaxy = catalogue.find_largest("ISOAREA_IMAGE")
    galaxy_eps = galaxy["ELLIPTICITY"]  # Mean galaxy elliptisity
    galaxy_pa = radians(galaxy["THETA_IMAGE"])  # Mean galaxy position angle
    galaxy_sma = galaxy["A_IMAGE"]  # Galaxy semi-major axis
    # galaxy_x0 = galaxy["X_IMAGE"]
    # galaxy_y0 = galaxy["Y_IMAGE"]

    min_aper_sma = args.min_aper  # Minimal aperture size
    # Maximal aperture size.
    if args.max_aper is None:
        max_aper_sma = max(x_size-x_cen, y_size-y_cen, x_cen, y_cen)
    else:
        max_aper_sma = args.max_aper

    # Create guess ellipse and fit it's center
    geometry = EllipseGeometry(x0=x_cen, y0=y_cen, sma=galaxy_sma/2, eps=galaxy_eps, pa=galaxy_pa)
    geometry.find_center(galaxy_masked_data, verbose=False)
    fitted_x0 = geometry.x0
    fitted_y0 = geometry.y0
    ellipse = Ellipse(galaxy_masked_data, geometry)

    # If mask file is specified and the mask is not empty, fit the galaxy
    # image with a set of ellipses, then use this model to fill in all the
    # masked out gaps.
    a3_last, b3_last, a4_last, b4_last = 0.0, 0.0, 0.0, 0.0
    pa_last, eps_last = galaxy_pa, galaxy_eps
    if (np.sum(mask_data) >= 0) or args.make_model or args.outer_isophote:
        # Fit ellipses to the data and fill the masked regions with
        # the mean values along the corresponding ellipse.
        print("Creating an ellipse-model of the galaxy...")
        print("# Sma    intensity")
        ellipse_list = IsophoteList([])
        for sma in np.arange(min_aper_sma, max_aper_sma+0.01, args.aper_step):
            fitted_ellipse = ellipse.fit_isophote(sma, minit=30, maxit=200, fflag=0.25, nclip=3)
            # Create an ellipse centered on the galaxy center and with parameters equal to
            # ones of the fitted ellipse, if the fitting was successful
            if (fitted_ellipse.stop_code == 0) and (fitted_ellipse.eps < 0.9):
                constructed_ellipse = SimpleNamespace(x0=fitted_x0, y0=fitted_y0, sma=sma, intens=0,
                                                      eps=fitted_ellipse.eps, pa=fitted_ellipse.pa,
                                                      grad=fitted_ellipse.grad, a3=fitted_ellipse.a3,
                                                      b3=fitted_ellipse.b3, a4=fitted_ellipse.a4,
                                                      b4=fitted_ellipse.b4, fit_ok=True)
                eps_last = fitted_ellipse.eps
                pa_last = fitted_ellipse.pa
                a3_last = fitted_ellipse.a3
                b3_last = fitted_ellipse.b3
                a4_last = fitted_ellipse.a4
                b4_last = fitted_ellipse.b4
            else:
                # The fitting was unsucessful. Let's use ellipse parameters of the last successful fit
                constructed_ellipse = SimpleNamespace(x0=fitted_x0, y0=fitted_y0, sma=sma, intens=0,
                                                      eps=eps_last, pa=pa_last, grad=0, a3=a3_last,
                                                      b3=b3_last, a4=a4_last, b4=b4_last, fit_ok=False)

            # Compute median intensity along the fitted ellipse instead of the default mean one
            # to suppress spurious ellipses
            if not is_ellipse_inside_frame(constructed_ellipse, x_size, y_size, margin=3):
                break
            _, intensity_along_ellipse, _ = get_inten_along_ellipse(constructed_ellipse, galaxy_data, mask_data)

            # Set the median intensity as the intensity along the ellipse
            constructed_ellipse.intens = np.median(intensity_along_ellipse)
            constructed_ellipse.intens_std = np.std(intensity_along_ellipse)

            if constructed_ellipse.intens <= 0:
                print("We have reached zero-intensity area. Exiting.")
                max_aper_sma = sma - args.aper_step
                break

            # If the outer isophote was not specified and we have reached the image border, then we stop iterate
            if (args.outer_isophote is None) and (not is_ellipse_inside_frame(constructed_ellipse, x_size, y_size)):
                max_aper_sma = sma - args.aper_step
                print("An image edge was reached. Stopping the iterration.")
                break

            # Add the ellipse to the list
            ellipse_list.append(constructed_ellipse)

            # If the outer isophote was specified, we won't stop untill it reached even if
            # we have reached the image borders already
            current_surf_bri = flux_to_surf_bri(constructed_ellipse.intens, mag_zpt, args.scale)
            if (args.outer_isophote is not None) and (current_surf_bri > args.outer_isophote) and\
               (not is_ellipse_inside_frame(constructed_ellipse, x_size, y_size)):
                print("Both outer isophote and image edge were reached.")
                break

            # Set guess values for the next ellipse
            if fitted_ellipse.stop_code == 0:
                # Fitting was successful. Fitted parameters can be used.
                geometry = EllipseGeometry(x0=fitted_ellipse.x0, y0=fitted_ellipse.y0, sma=fitted_ellipse.sma,
                                           eps=fitted_ellipse.eps, pa=fitted_ellipse.pa)
            else:
                # Fitting was unsuccessful. Galaxy-average parameters should be used instead
                geometry = EllipseGeometry(x0=fitted_x0, y0=fitted_y0, sma=sma, eps=galaxy_eps, pa=galaxy_pa)
            ellipse = Ellipse(galaxy_masked_data, geometry)
            print("%7.2f %8.2f" % (sma, constructed_ellipse.intens))

        # Extract essential parameters of fitted ellipses and interpolate them
        ells_smas = [ell.sma for ell in ellipse_list]
        ells_intensities = [ell.intens for ell in ellipse_list]
        ells_intensities_stds = [ell.intens_std for ell in ellipse_list]
        ells_epses = [ell.eps for ell in ellipse_list]
        ells_epses_interpolated = interp1d(ells_smas, ells_epses, kind="cubic", bounds_error=False)
        ells_pas = [ell.pa for ell in ellipse_list]
        ells_pas_interpolated = interp1d(ells_smas, ells_pas, kind="cubic", bounds_error=False)
        ells_b4s = [ell.b4 for ell in ellipse_list]

        ells_surf_brightnesses = flux_to_surf_bri(ells_intensities, mag_zpt, args.scale)
        ells_surf_brightnesses_interpolated = interp1d(ells_smas, ells_surf_brightnesses, kind="cubic",
                                                       bounds_error=False)
        if args.outer_isophote is not None:
            # Find parameters of an outer isophote
            if args.outer_isophote < float(ells_surf_brightnesses_interpolated(ells_smas[-1])):
                outer_isophote_sma = brentq(lambda x: float(ells_surf_brightnesses_interpolated(x)) - args.outer_isophote,
                                            a=ells_smas[0], b=ells_smas[-1])
            else:
                outer_isophote_sma = ells_smas[-1]
            # Find other parameters of outer isophote
            outer_isophote_eps = float(ells_epses_interpolated(outer_isophote_sma))
            outer_isophote_pa = float(ells_pas_interpolated(outer_isophote_sma))
            if args.scale < 0:
                print("Semi-major axis of %1.2f isophote = %1.2f [pix]" % (args.outer_isophote, outer_isophote_sma))
            else:
                print("Semi-major axis of %1.2f isophote = %1.2f ['']" % (args.outer_isophote,
                                                                          outer_isophote_sma*args.scale))

        # Build a model using only ellipses that fit inside the image
        print("\nBuilding a model", end="")
        good_ells = []
        for ell in ellipse_list:
            if is_ellipse_inside_frame(ell, x_size, y_size, margin=5):
                good_ells.append(ell)
        print(" with outer isophote sma = %1.2f..." % good_ells[-1].sma)
        good_ells = IsophoteList(good_ells)
        model_data = build_ellipse_model(galaxy_data.shape, good_ells)
        print("Done")

        # Fill the masked regions in the image with model data
        galaxy_filled_data = galaxy_data.copy()
        galaxy_filled_data[mask_data > 0] = model_data[mask_data > 0]

        # Save the model and the filled images
        fits.PrimaryHDU(data=galaxy_filled_data).writeto(args.out_dir/"filled.fits", overwrite=True)
        fits.PrimaryHDU(data=model_data).writeto(args.out_dir/"model.fits", overwrite=True)

    # Create a list of elliptical apertures centered on the galaxy, with ellipticity eqial to
    # the galaxy ellipticity and with radii from 1 pixel to max of the max_aper_sma
    aper_radii = np.linspace(ells_smas[0], ells_smas[-1], int(ells_smas[-1] / args.aper_step))
    apertures = [EllipticalAperture(positions=(fitted_x0, fitted_y0),
                                    a=sma,
                                    b=sma*(1-float(ells_epses_interpolated(sma))),
                                    theta=float(ells_pas_interpolated(sma))) for sma in aper_radii]
    # Perform elliptical aperture photometry
    print("\nPerforming the aperture photometry...", end="")
    phot_table = aperture_photometry(galaxy_filled_data, apertures)
    aper_fluxes = list(phot_table.as_array()[0])[3:]
    full_flux = aper_fluxes[-1]

    # Let's find some interesting apertures: aperture at which 0.25, 0.5, 0.75, 0.95 of the total flux occures
    aper_fluxes_interpolated = interp1d(aper_radii, aper_fluxes, kind="cubic", bounds_error=False)
    aper_sma_25 = brentq(lambda x: float(aper_fluxes_interpolated(x))-0.25*full_flux,
                         a=aper_radii[0], b=aper_radii[-1])
    aper_sma_50 = brentq(lambda x: float(aper_fluxes_interpolated(x))-0.5*full_flux,
                         a=aper_radii[0], b=aper_radii[-1])
    aper_sma_75 = brentq(lambda x: float(aper_fluxes_interpolated(x))-0.75*full_flux,
                         a=aper_radii[0], b=aper_radii[-1])
    aper_sma_95 = brentq(lambda x: float(aper_fluxes_interpolated(x))-0.95*full_flux,
                         a=aper_radii[0], b=aper_radii[-1])
    print("Done.")

    # Make a plot with the galaxy and isophotes
    # Show galaxy on the plot
    plt.imshow(galaxy_filled_data, origin="lower", norm=LogNorm())
    # Mark apertures on the plot
    EllipticalAperture(positions=(fitted_x0, fitted_y0), a=aper_sma_25,
                       b=aper_sma_25*(1-float(ells_epses_interpolated(aper_sma_25))),
                       theta=float(ells_pas_interpolated(aper_sma_25))).plot(label=r"$r_{0.25}$", linewidth=1.5, color="r")
    EllipticalAperture(positions=(fitted_x0, fitted_y0), a=aper_sma_50,
                       b=aper_sma_50*(1-float(ells_epses_interpolated(aper_sma_50))),
                       theta=float(ells_pas_interpolated(aper_sma_50))).plot(label=r"$r_{0.50}$", linewidth=1.25, color="g")
    EllipticalAperture(positions=(fitted_x0, fitted_y0), a=aper_sma_75,
                       b=aper_sma_75*(1-float(ells_epses_interpolated(aper_sma_75))),
                       theta=float(ells_pas_interpolated(aper_sma_75))).plot(label=r"$r_{0.75}$", linewidth=0.75, color="b")
    EllipticalAperture(positions=(fitted_x0, fitted_y0), a=aper_sma_95,
                       b=aper_sma_95*(1-float(ells_epses_interpolated(aper_sma_95))),
                       theta=float(ells_pas_interpolated(aper_sma_95))).plot(label=r"$r_{0.95}$", linewidth=0.5, color="m")
    if args.outer_isophote is not None:
        EllipticalAperture(positions=(fitted_x0, fitted_y0), a=outer_isophote_sma,
                           b=outer_isophote_sma*(1-outer_isophote_eps),
                           theta=outer_isophote_pa).plot(label=r"$r_{\mu=%1.2f}$" % args.outer_isophote,
                                                         linestyle="--", color="cyan")

    apertures[-1].plot(label=r"$r_{lim}$", linestyle=":")  # Outer aperture
    plt.legend()
    plt.savefig(args.out_dir/"apertures.png")
    plt.savefig(args.out_dir/"apertures.eps")
    plt.clf()

    # Save results for several apertures to an ASCII file
    fout = open(args.out_dir/"apertures.dat", "w")
    fout.truncate(0)
    # Make header
    fout.write("# Ellipse  sma[pix]  surf_bri   mag_aper   ell   posang[deg]\n")
    aper_list = [aper_sma_25, aper_sma_50, aper_sma_75, aper_sma_95, apertures[-1].a]
    aper_names = ["r_0.25", "r_0.50", "r_0.75", "r_0.95", "r_lim "]
    for aper, name in zip(aper_list, aper_names):
        fout.write("%s %9.2f %9.2f " % (name, aper, float(ells_surf_brightnesses_interpolated(aper))))
        fout.write("%9.2f " % flux_to_mag(float(aper_fluxes_interpolated(aper)), mag_zpt))
        fout.write("%9.3f " % float(ells_epses_interpolated(aper)))
        fout.write("%9.2f\n" % float(degrees(ells_pas_interpolated(aper))))
    if args.outer_isophote is not None:
        fout.write("r_out  %9.2f %9.2f " % (outer_isophote_sma, args.outer_isophote))
        fout.write("%9.2f " % flux_to_mag(float(aper_fluxes_interpolated(outer_isophote_sma)), mag_zpt))
        fout.write("%9.3f " % outer_isophote_eps)
        fout.write("%8.2f\n" % degrees(outer_isophote_pa))
    fout.close()

    # Save ellipses parameters as a function of SMA
    fout = open(args.out_dir/"ellipses.dat", "w")
    fout.truncate(0)
    fout.write("# sma[pix]  flux   mag_aper   ell   posang[deg]   A4     B4    fit_status\n")
    for ell in ellipse_list:
        fout.write("%7.2f %13.6e" % (ell.sma, ell.intens))
        aperture = EllipticalAperture(positions=(fitted_x0, fitted_y0), a=ell.sma,
                                      b=ell.sma*(1-ell.eps), theta=ell.pa)
        phot_table = aperture_photometry(galaxy_filled_data, [aperture])
        aper_flux = list(phot_table.as_array()[0])[3]
        aper_mag = flux_to_mag(aper_flux, mag_zpt)
        fout.write("%8.2f " % aper_mag)
        fout.write("%8.3f %7.2f " % (ell.eps, degrees(ell.pa)))
        fout.write("%9.3f %6.3f   " % (ell.a4, ell.b4))
        fout.write("success" if ell.fit_ok is True else "fail")
        fout.write("\n")
    fout.close()

    # Make a plot with a growth curve
    plt.plot(abs(args.scale) * aper_radii, flux_to_mag(aper_fluxes, mag_zpt))
    plt.plot(abs(args.scale) * aper_radii, flux_to_mag(aper_fluxes, mag_zpt), "go")
    plt.axvline(x=abs(args.scale)*aper_sma_25, label=r"$r_{0.25}$", linewidth=1.5)
    plt.axvline(x=abs(args.scale)*aper_sma_50, label=r"$r_{0.50}$", linewidth=1.25)
    plt.axvline(x=abs(args.scale)*aper_sma_75, label=r"$r_{0.75}$", linewidth=0.75)
    plt.axvline(x=abs(args.scale)*aper_sma_95, label=r"$r_{0.95}$", linewidth=0.5)
    plt.axvline(x=abs(args.scale)*apertures[-1].a, label=r"$r_{lim}$", linestyle=":")
    if args.outer_isophote is not None:
        plt.axvline(x=abs(args.scale)*outer_isophote_sma, linestyle="--",
                    label=r"$r_{\mu=%1.2f}$" % args.outer_isophote)
    plt.gca().invert_yaxis()
    if args.scale < 0:
        plt.xlabel("Aperture sma [pix]")
    else:
        plt.xlabel("Aperture SMA ['']")
    plt.ylabel("mag")
    plt.legend()
    plt.savefig(args.out_dir/"growth_curve.png")
    plt.savefig(args.out_dir/"growth_curve.eps")
    plt.clf()

    # Make a plot with azimuthally averaged profile
    ells_surf_brightnesses_stds = 1.085 * np.array(ells_intensities_stds) / np.array(ells_intensities)
    ells_surf_brightnesses_stds[ells_surf_brightnesses_stds > 1.0] = 1.0
    plt.errorbar(x=abs(args.scale)*np.array(ells_smas),
                 y=ells_surf_brightnesses,
                 yerr=ells_surf_brightnesses_stds,
                 linestyle="-", color="k", marker="o")
    plt.axvline(x=abs(args.scale)*aper_sma_25, label=r"$r_{0.25}$", linewidth=1.5, color="k")
    plt.axvline(x=abs(args.scale)*aper_sma_50, label=r"$r_{0.50}$", linewidth=1.25, color="k")
    plt.axvline(x=abs(args.scale)*aper_sma_75, label=r"$r_{0.75}$", linewidth=0.75, color="k")
    plt.axvline(x=abs(args.scale)*aper_sma_95, label=r"$r_{0.95}$", linewidth=0.5, color="k")
    plt.axvline(x=abs(args.scale)*apertures[-1].a, label=r"$r_{lim}$", linestyle=":", color="k")
    if args.outer_isophote is not None:
        plt.axvline(x=abs(args.scale)*outer_isophote_sma, linestyle="--",
                    label=r"$r_{\mu=%1.2f}$" % args.outer_isophote, color="k")
    plt.gca().invert_yaxis()
    if args.scale < 0:
        plt.xlabel("SMA [pix]")
    else:
        plt.xlabel("SMA ['']")
    plt.ylabel("mag / sq. arcsec")
    plt.legend()
    plt.savefig(args.out_dir/"azim_profile.png")
    plt.savefig(args.out_dir/"azim_profile.eps")
    plt.clf()

    # Make a plot with ellipses parameters as a function of SMA
    plt.figure(figsize=(10, 10))
    plt.subplot(311)
    plt.plot(ells_smas, ells_epses, "ro")
    plt.ylabel(r"$\epsilon$")
    plt.subplot(312)
    plt.plot(ells_smas, np.degrees(ells_pas), "ro")
    plt.ylabel(r"$PA$")
    plt.subplot(313)
    plt.plot(ells_smas, ells_b4s, "ro")

    plt.xlabel("$SMA$")
    plt.ylabel(r"$B_4$")
    plt.savefig(args.out_dir/"ellipses.png")
    plt.savefig(args.out_dir/"ellipses.eps")
    plt.clf()

    logfile.write("Success\n")
    logfile.close()


if __name__ == '__main__':
    # Parsing command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=pathlib.Path,
                        help="Galaxy image")
    parser.add_argument("--mask", type=pathlib.Path,
                        help="Mask image (zero pixels are good).")
    parser.add_argument("--x-cen", type=float,
                        help="X-coordinate of galaxy center. Default: image center.")
    parser.add_argument("--y-cen", type=float,
                        help="Y-coordinate of galaxy center. Default: image center.")
    parser.add_argument("--out-dir", type=pathlib.Path,
                        default="result", help="Directory with results")
    parser.add_argument("--min-aper", type=float, default=10.0,
                        help="Minimum semi-major size of the aperture [pix]. Default: 10 pixels.")
    parser.add_argument("--max-aper", type=float, default=None,
                        help="Maximum semi-major size of the aperture. Default: image limited aperture.")
    parser.add_argument("--mag-zpt", type=float, default=None,
                        help="Magnitude zeropoint. Default: value from the fits header.")
    parser.add_argument("--outer-isophote", type=float, default=None,
                        help="Magnitude of an outer isophote. Default: no outer isophote.")
    parser.add_argument("--aper-step", type=float, default=5.0,
                        help="Aperture step in pixels. Default 5 pix.")
    parser.add_argument("--make-model", default=False, action="store_true")
    parser.add_argument("--scale", default=-1, type=float, help="Scale arcsecs per pixel")
    args = parser.parse_args()
    main(args)
